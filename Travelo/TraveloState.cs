﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using Microsoft.Bot.Builder.Dialogs;

namespace Travelo
{
    public class TraveloState: DialogState
    {
        public string Destination { get; set; }
        public string Origin { get; set; }
        public string FlyDate { get; set; }
        public string ReturnDate { get; set; }
       
    }
}