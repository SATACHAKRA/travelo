﻿using Microsoft.Bot.Builder.Dialogs;
using System.Collections.Generic;

namespace Travelo
{
    public class HelpDialog:WaterfallDialog
    {
        public HelpDialog(string dialogId, IEnumerable<WaterfallStep> steps = null) : base(dialogId, steps)
        {
            AddStep(async (stepContext, cancellationToken) =>
            {

                await stepContext.Context.SendActivityAsync("For any kind of assistance you can call at 📞 +1 (888) 889 - 8005. We are available 24X7 for your assistance.");
                return await stepContext.EndDialogAsync();


            });
        }

        public static string Id => "HelpDialog";

        public static HelpDialog Instance { get; } = new HelpDialog(Id);
    }
}
