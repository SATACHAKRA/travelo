﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

using System;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;

namespace Travelo
{
    
    public class TraveloAccessors
    {
       
        public TraveloAccessors(ConversationState conversationState)
        {
            ConversationState = conversationState ?? throw new ArgumentNullException(nameof(conversationState));
        }

        /// <summary>
        /// Gets the <see cref="IStatePropertyAccessor{T}"/> name used for the <see cref="CounterState"/> accessor.
        /// </summary>
        /// <remarks>Accessors require a unique name.</remarks>
        /// <value>The accessor name for the counter accessor.</value>
        public static string CounterStateName { get; } = $"{nameof(TraveloAccessors)}.CounterState";

        
        public IStatePropertyAccessor<CounterState> CounterState { get; set; }
        
        public ConversationState ConversationState { get; }

        public static string TraveloStateAccessorName { get; } = $"{nameof(TraveloAccessors)}.TraveloState";
        public IStatePropertyAccessor<TraveloState> TraveloStateStateAccessor { get; set; }

        public static string DialogStateAccessorName { get; } = $"{nameof(TraveloAccessors)}.DialogState";
        public IStatePropertyAccessor<DialogState> DialogStateAccessor { get; set; }

        
 
    }
}
