﻿using Microsoft.Bot.Builder.Dialogs;
using System.Collections.Generic;

namespace Travelo
{
    public class thanksDialog:WaterfallDialog
    {
        public thanksDialog(string dialogId, IEnumerable<WaterfallStep> steps = null) : base(dialogId, steps)
        {
            AddStep(async (stepContext, cancellationToken) =>
            {

                await stepContext.Context.SendActivityAsync("I am happy that i was able to help you. What else can I help you with ? ");
                return await stepContext.EndDialogAsync();


            });
        }

        public static string Id => "thanksDialog";

        public static thanksDialog Instance { get; } = new thanksDialog(Id);
    }
}
