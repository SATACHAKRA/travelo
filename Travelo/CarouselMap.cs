﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Travelo
{
     public partial class CarouselMap
    {
        [JsonProperty("status")]
        public bool Status { get; set; }

        [JsonProperty("statusCode")]
        public long StatusCode { get; set; }

        [JsonProperty("statusMessage")]
        public object StatusMessage { get; set; }

        [JsonProperty("data")]
        public Datum[] Data { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("imageURL")]
        public Uri ImageUrl { get; set; }

        [JsonProperty("apiURL")]
        public Uri ApiUrl { get; set; }

        [JsonProperty("bookURL")]
        public object BookUrl { get; set; }

        [JsonProperty("searchId")]
        public object SearchId { get; set; }

        [JsonProperty("flightId")]
        public string FlightId { get; set; }

        //[JsonProperty("cardType")]
        //[JsonConverter(typeof(ParseStringConverter))]
        //public long CardType { get; set; }

        [JsonProperty("linkURL")]
        public Uri LinkUrl { get; set; }
    }
}
