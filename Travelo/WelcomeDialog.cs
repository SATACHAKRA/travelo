﻿using Microsoft.Bot.Builder.Dialogs;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;


namespace Travelo
{
    internal class WelcomeDialog : WaterfallDialog
    {
        public WelcomeDialog(string dialogId, IEnumerable<WaterfallStep> steps = null) : base(dialogId, steps)
        {
            AddStep(async (stepContext, cancellationToken) =>
            {
                await SendWelcomeMessageAsync(stepContext, cancellationToken);
                return await stepContext.EndDialogAsync();
            });


        }

        public static string Id => "WelcomeDialog";

        public static WelcomeDialog Instance { get; } = new WelcomeDialog(Id);

        //Welcome message part is handled here
        public static async Task SendWelcomeMessageAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            foreach (var member in stepContext.Context.Activity.MembersAdded)
            {
                if (member.Id != stepContext.Context.Activity.Recipient.Id)
                {
                    stepContext.Context.SendActivityAsync($"Welcome to Travelo's. How can I help you with your travel needs today?");

                }
            }

        }
    }
}
