﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.


using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.BotBuilderSamples;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

using RestSharp.Extensions;

namespace Travelo
{
    
    public class TraveloBot : IBot
    {
        private readonly TraveloAccessors _accessors;
        private readonly ILogger _logger;
        public TraveloAccessors TraveloAccessors { get; }
        //public static Dictionary<string, EntityMap> dialogArgs = new Dictionary<string, EntityMap>();

        public static Dictionary<string, string> dialogArgs = new Dictionary<string, string>();
        private readonly DialogSet dialogs;
        private readonly BotServices _services;

        public static readonly string LuisKey = "Travelo";

        
        public TraveloBot(TraveloAccessors accessors, ILoggerFactory loggerFactory, BotServices services)
        {
            if (loggerFactory == null)
            {
                throw new System.ArgumentNullException(nameof(loggerFactory));
            }

            _logger = loggerFactory.CreateLogger<TraveloBot>();
            _logger.LogTrace("Turn start.");
            _accessors = accessors ?? throw new System.ArgumentNullException(nameof(accessors));

            var dialogState = accessors.DialogStateAccessor;
            dialogs = new DialogSet(dialogState);
            _services = services;
            dialogs.Add(new MainDialog(MainDialog.Id, dialogArgs));
            dialogs.Add(WelcomeDialog.Instance);
            dialogs.Add(greetingsDialog.Instance);
            dialogs.Add(thanksDialog.Instance);
            dialogs.Add(HelpDialog.Instance);
            dialogs.Add(EndDialog.Instance);
            dialogs.Add(new ChoicePrompt("choicePrompt"));
            dialogs.Add(new TextPrompt("textPrompt"));
            dialogs.Add(new DateTimePrompt("datePrompt",DateValidatorAsync));
            TraveloAccessors = accessors;


        }

        public TraveloBot(BotServices services)
        {
            _services = services ?? throw new System.ArgumentNullException(nameof(services));
            if (!_services.LuisServices.ContainsKey(LuisKey))
            {
                throw new System.ArgumentException($"Invalid configuration. Please check your '.bot' file for a LUIS service named '{LuisKey}'.");
            }
        }

        public async Task OnTurnAsync(ITurnContext turnContext, CancellationToken cancellationToken = default(CancellationToken))
        {         
           if (turnContext.Activity.Type == ActivityTypes.ConversationUpdate)
            {
                turnContext.TurnState.Add("TraveloAccessors", TraveloAccessors);

                var dialogCtx = await dialogs.CreateContextAsync(turnContext, cancellationToken);

                if (dialogCtx.ActiveDialog == null)
                {

                    await dialogCtx.BeginDialogAsync(WelcomeDialog.Id, cancellationToken);

                }
            }
            
            ///////////////////
            
           else if (turnContext.Activity.Type == ActivityTypes.Message)
           {
                turnContext.TurnState.Add("TraveloAccessors", TraveloAccessors);
                var dc = await dialogs.CreateContextAsync(turnContext, cancellationToken);

                if (dc.ActiveDialog == null)
                {
                    //////////////////////////// Cases Begin Here//////////////////////
                    ///This Portion makes call to The Custon Extract API 
                    
                    var userQuery = turnContext.Activity.Text;
                    var client = new RestClient("https://api.travelomind.com/");
                    var request = new RestRequest("api/luis/home/extract?q="+ userQuery, Method.GET);
                    var queryResult = client.Execute(request).Content;

                    ////// Print the Query result out here

                    // await turnContext.SendActivityAsync(queryResult);


                    EntityMap Entdic1 = JsonConvert.DeserializeObject<EntityMap>(queryResult);

                    //var guid = Guid.NewGuid().ToString();
                    //dialogArgs.Remove("1");
                    //dialogArgs.Add("1", Entdic1);


                    dialogArgs.Clear();

                    if (Entdic1.Entities != null)
                    {
                        foreach (Entity entity in Entdic1.Entities)
                        {
                            dialogArgs.Add(entity.Type, entity.EntityEntity);

                        }
                    }

                    // Using Jobject method the Top Intent can also be identified

                    // var obj = JObject.Parse(queryResult);
                    //var topIntent = (string)obj["topScoringIntent"]["intent"];


                    //var recognizerResult = await _services.LuisServices[LuisKey].RecognizeAsync(turnContext, cancellationToken);
                    //Newtonsoft.Json.Linq.JObject Entities1 = recognizerResult?.Entities;



                    ////////////// Checking the Top Intent Out here first ////////////

                    var topIntent = Entdic1.TopScoringIntent.Intent;

                    if (topIntent != null)
                    {
                        //    //await turnContext.SendActivityAsync($"==>LUIS Top Scoring Intent: {topIntent.Value.intent}, Score: {topIntent.Value.score}\n.");

                        //    //await turnContext.SendActivityAsync($"\n  Entities Returned {Entities1}");                                             

                        switch (topIntent)
                        {
                            case "flight":                               
                                await dc.BeginDialogAsync(MainDialog.Id, dialogArgs);
                                break;
                            case "greetings":
                                await dc.BeginDialogAsync(greetingsDialog.Id);
                                break;
                            case "Help":
                                await dc.BeginDialogAsync(HelpDialog.Id);
                                break;
                            case "thanks":
                                await dc.BeginDialogAsync(thanksDialog.Id);
                                break;
                            case "None":
                            default:
                                await dc.Context.SendActivityAsync($"Sorry, I haven't been trained for this.");
                                await dc.EndDialogAsync();
                                break;
                        }


                    }

                    else
                    {
                        var msg = @"Sorry...Not trained for this.";
                        await turnContext.SendActivityAsync(msg);
                    }

                    /////////////////luis cases end here////////////////////
                }
                else
                {
                    string utterance = turnContext.Activity.Text.Trim().ToLowerInvariant();
                    if (utterance.Contains("help") || utterance.Contains("assit") || utterance.Contains("stuck") || utterance.Contains("confused"))
                    {
                        var reply1 = turnContext.Activity.CreateReply();

                        // Create an attachment.
                        var attachment = new Attachment
                        {
                            ContentUrl = "http://www.transagra.com/img/spot/How-can-we-help.jpg",
                            ContentType = "image/jpg",
                            Name = "Cancel",
                        };

                        reply1.Attachments = new List<Attachment>() { attachment };

                        // Create an attachment.
                        //                    var activity = MessageFactory.Carousel(new Attachment[]
                        //{
                        //    new HeroCard(
                        //        title: "We are a travel agency trusted over 30 years, with 95 % positive customer reviews and A+ rating from BBB",
                        //        images: new CardImage[] { new CardImage(url: "https://rlv.zcache.com.au/ask_me_i_am_happy_to_help_button-r3c93e701084a405e83c37146a7618395_k94r7_540.jpg?rlvnet=1") },
                        //        buttons: new CardAction[]
                        //        {
                        //            new CardAction(title: "🤖 Chat with Bot ", type: ActionTypes.ImBack, value: "Book some flight tickets"),
                        //            new CardAction(title: "☎️ Call Us 24/7", type: ActionTypes.Call, value: "+18888898005"),
                        //            new CardAction(title: "✈️ Visit Website", type: ActionTypes.OpenUrl, value: "www.travelomind.com")

                        //        })
                        //    .ToAttachment(),
                        //    new HeroCard(
                        //        title: "We are a travel agency trusted over 30 years, with 95 % positive customer reviews and A+ rating from BBB",
                        //        images: new CardImage[] { new CardImage(url: "https://rlv.zcache.co.uk/ask_me_i_am_happy_to_help_yellow_black_badge-ra1a6c6127c004ddd8e3bcf52c6532476_k94r7_540.jpg?rlvnet=1") },
                        //        buttons: new CardAction[]
                        //        {
                        //           new CardAction(title: "🤖 Chat with Bot ", type: ActionTypes.ImBack, value: "Book some flight tickets"),
                        //            new CardAction(title: "☎️ Call Us 24/7", type: ActionTypes.Call, value: "+18888898005"),
                        //            new CardAction(title: "✈️ Visit Website", type: ActionTypes.OpenUrl, value: "www.travelomind.com")
                        //        })
                        //    .ToAttachment()

                        //});


                        //                    //reply1.Attachments = new List<Attachment>() { attachment };

                        await turnContext.SendActivityAsync("For any kind of assistance you can call at 📞 +1 (888) 889 - 8005. We are available 24X7 for your assistance.");
                        //await turnContext.SendActivityAsync(activity, cancellationToken: cancellationToken);
                        ///////////////////////////////////////////////////
                        //Send image as carousel.
                        //
                        await turnContext.SendActivityAsync(reply1, cancellationToken: cancellationToken);
                        await dc.CancelAllDialogsAsync(cancellationToken);
                    

                    }
                    else if (utterance.Contains("cancel") || utterance.Contains("stop") || utterance.Contains("abort"))
                    {
                        var reply = turnContext.Activity.CreateReply();

                        // Create an attachment.
                        var attachment = new Attachment
                        {
                            ContentUrl = "https://rlv.zcache.com.au/ask_me_i_am_happy_to_help_button-r3c93e701084a405e83c37146a7618395_k94r7_540.jpg?rlvnet=1",
                            ContentType = "image/jpg",
                            Name = "Cancel",
                        };
                       
                        reply.Attachments = new List<Attachment>() { attachment };

                        await turnContext.SendActivityAsync(" You chose to cancel this operation. What can I do for you? 🤔 ");
                        
                        /////Send image as carousel.
                        await turnContext.SendActivityAsync(reply, cancellationToken: cancellationToken);
                        await dc.CancelAllDialogsAsync(cancellationToken);

                    }
                    else
                    {
                        await dc.ContinueDialogAsync(cancellationToken);
                        
                        if (!turnContext.Responded)
                        {
                            await dc.CancelAllDialogsAsync(cancellationToken);
                            await dc.BeginDialogAsync(greetingsDialog.Id, null, cancellationToken);
                        }
                    }

                    //await dc.ContinueDialogAsync(cancellationToken);
                }

              

            }

            else
            {
                await turnContext.SendActivityAsync($"{turnContext.Activity.Type} event detected");
            }
          
            ///////////////////////Save the conversation state/////////////////////////////////

            await TraveloAccessors.ConversationState.SaveChangesAsync(turnContext, false, cancellationToken);

        }


        /// <summary>
        /// //////////////Date Validator in use here//////////////
        /// </summary>
        /// <param name="promptContext"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>

        private async Task<bool> DateValidatorAsync(
           PromptValidatorContext<IList<DateTimeResolution>> promptContext,
           CancellationToken cancellationToken = default(CancellationToken))
        {
            // Check whether the input could be recognized as an integer.
            if (!promptContext.Recognized.Succeeded)
            {
                await promptContext.Context.SendActivityAsync(
                    "I'm sorry, I do not understand. Please enter the proper date for your reservation.",
                    cancellationToken: cancellationToken);
                return false;
            }

            // Check whether any of the recognized date-times are appropriate,
            // and if so, return the first appropriate date-time.
            DateTime earliest = DateTime.Now.AddHours(1.0);
            DateTimeResolution value = promptContext.Recognized.Value.FirstOrDefault(v =>
                DateTime.TryParse(v.Value ?? v.Start, out DateTime time) && DateTime.Compare(earliest, time) <= 0);

            
            if (value != null)
            {
                var usCulture = "en-US";
                var dateValue = DateTime.Parse(value.Value, new CultureInfo(usCulture, false));
                value.Value = dateValue.ToString("MM/dd/yyyy");                
                promptContext.Recognized.Value.Clear();
                promptContext.Recognized.Value.Add(value);
                return true;
            }

            await promptContext.Context.SendActivityAsync(
                    "I'm sorry, wrong input.",
                    cancellationToken: cancellationToken);
            return false;
        }
    }
}
