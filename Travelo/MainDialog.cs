﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Dialogs.Choices;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Linq;


namespace Travelo
{
    public class MainDialog : WaterfallDialog
    {
        public static readonly string Returndatementioned;
        IEnumerable<WaterfallStep> steps = null;
        string dialogID = null;
        public TraveloAccessors TraveloAccessors { get;  }
        

        public MainDialog(string dialogId,Dictionary<string, string> dic,IEnumerable<WaterfallStep> steps = null) : base(dialogId, steps)
        {          
                    
                AddStep(async (stepContext, cancellationToken) =>
                {
           
                    var state = await (stepContext.Context.TurnState["TraveloAccessors"] as TraveloAccessors).TraveloStateStateAccessor.GetAsync(stepContext.Context, new System.Func<TraveloState>(() => new TraveloState()));
                    if (dic.ContainsKey("source_city"))
                    {
                        state.Origin = dic["source_city"];
                        return await stepContext.ContinueDialogAsync();
                    }
                    else
                    {
                        return await stepContext.PromptAsync("textPrompt",
                        new PromptOptions
                        {
                            Prompt = stepContext.Context.Activity.CreateReply("So you want to book flight tickets. Where would you like to fly from ?"),

                        });
                    }

                });
                AddStep(async (stepContext, cancellationToken) =>
                {
                    var state = await (stepContext.Context.TurnState["TraveloAccessors"] as TraveloAccessors).TraveloStateStateAccessor.GetAsync(stepContext.Context, new System.Func<TraveloState>(() => new TraveloState()));
                    if (dic.ContainsKey("destination_city"))
                    {
                        state.Destination = dic["destination_city"];
                        if (state.Origin == null)
                        {
                            state.Origin = stepContext.Result.ToString();
                        }
                        return await stepContext.ContinueDialogAsync();
                    }
                    else
                    {
                        if (state.Origin == null)
                        {
                            state.Origin = stepContext.Result.ToString();
                        }
                        return await stepContext.PromptAsync("textPrompt",
                        new PromptOptions
                        {
                            Prompt = stepContext.Context.Activity.CreateReply($"Origin:{state.Origin}. What is your destination ?"),

                        });
                    }

              

                });
                AddStep(async (stepContext, cancellationToken) =>
                {
                    var state = await (stepContext.Context.TurnState["TraveloAccessors"] as TraveloAccessors).TraveloStateStateAccessor.GetAsync(stepContext.Context, new System.Func<TraveloState>(() => new TraveloState()));
                    if (dic.ContainsKey("fly_date"))
                    {
                        state.FlyDate = dic["fly_date"];
                        if (state.Destination == null)
                        {
                            state.Destination = stepContext.Result.ToString();
                        }
                        return await stepContext.ContinueDialogAsync();
                    }
                    else
                    {
                        if (state.Destination == null)
                        {
                            state.Destination = stepContext.Result.ToString();
                        }
                        return await stepContext.PromptAsync("datePrompt",
                        new PromptOptions
                        {
                            Prompt = stepContext.Context.Activity.CreateReply($"Destination: {state.Destination}.Origin: {state.Origin}. What date are you flying ?"),

                        });
                    }

                });

                AddStep(async (stepContext, cancellationToken) =>
                {
                    var state = await (stepContext.Context.TurnState["TraveloAccessors"] as TraveloAccessors).TraveloStateStateAccessor.GetAsync(stepContext.Context, new System.Func<TraveloState>(() => new TraveloState()));
                    if (dic.ContainsKey("return_date"))
                    {
                        state.ReturnDate = dic["return_date"];

                        if (state.FlyDate == null)
                        {
                            state.FlyDate = ((List<DateTimeResolution>)(stepContext.Result))[0].Value; ;
                        }
                        return await stepContext.ContinueDialogAsync();
                    }
                    else
                    {
                        if (state.FlyDate == null)
                        {
                            state.FlyDate = ((List<DateTimeResolution>)(stepContext.Result))[0].Value; ;
                        }
                                               
                        
                        ////////////////////// Asking whether user wants return ticket or not ////////////////////
                        
                        return await stepContext.PromptAsync("choicePrompt",
                        new PromptOptions
                        {
                            Prompt = stepContext.Context.Activity.CreateReply($"Destination: {state.Destination}.Origin: {state.Origin} Flight date: {state.FlyDate}. Would you like to get a roundtrip ticket?"),
                            Choices = new[] { new Choice { Value = "Yes" }, new Choice { Value = "No" } }.ToList()
                        });
                    }
                                       
                });

            AddStep(async (stepContext, cancellationToken) =>
            {
                var state = await (stepContext.Context.TurnState["TraveloAccessors"] as TraveloAccessors).TraveloStateStateAccessor.GetAsync(stepContext.Context, new System.Func<TraveloState>(() => new TraveloState()));
                var response = (stepContext.Result as FoundChoice)?.Value;
                
                 if (response == "Yes")
                 {

                    return await stepContext.PromptAsync("datePrompt",
                    new PromptOptions
                    {
                        Prompt = stepContext.Context.Activity.CreateReply($"Destination:{state.Destination}.Origin:{state.Origin}, Fly Date:{state.FlyDate}.\n What is your return date ?"),

                    });
                    
                  }

                if (response == "No")
               {                    
                    await stepContext.Context.SendActivityAsync($"Let us show you the best deals we have got for your One-way trip from {state.Origin} to {state.Destination} on {state.FlyDate}, per traveler in Economy class");
                    ///
                    //////////////////////////////////////////        
                    /// making an api call to retrieve carousels for one way trip.
                    /// 
                    var client = new RestClient("https://stagingapi.travelomind.com/");
                    var request = new RestRequest($"api/Flights/carousel/summary?to={state.Destination}&from={state.Origin}&departDate={state.FlyDate}&returnDate=&adult=1&child=0&infant=0&queryType=1&roundTrip=FALSE&classOfTravel=Y&userName=GOD", Method.GET);
                    var queryResult = client.Execute(request).Content;
                    CarouselMap CarouselResult = JsonConvert.DeserializeObject<CarouselMap>(queryResult);

                    if (CarouselResult.Status)
                    {
                        // Create an attachment. Add the carousels to it.

                        var activity1 = MessageFactory.Carousel(new Attachment[]
                    {
                    new HeroCard(
                        title: "We are a travel agency trusted over 30 years, with 95 % positive customer reviews and A+ rating from BBB",
                        images: new CardImage[] { new CardImage(url: CarouselResult.Data[0].ImageUrl.ToString())},
                        buttons: new CardAction[]
                        {
                            new CardAction(title: "🤖 Chat with Bot ", type: ActionTypes.ImBack, value: "Book some flight tickets"),
                            new CardAction(title: "☎️ Call Us 24/7", type: ActionTypes.Call, value: "+18888898005"),
                            new CardAction(title: "✈️ Search Results ", type: ActionTypes.OpenUrl, value: CarouselResult.Data[0].ApiUrl.ToString())

                        })
                    .ToAttachment(),
                    new HeroCard(
                        title: "We are a travel agency trusted over 30 years, with 95 % positive customer reviews and A+ rating from BBB",
                        images: new CardImage[] { new CardImage(url: CarouselResult.Data[1].ImageUrl.ToString()) },
                        buttons: new CardAction[]
                        {
                           new CardAction(title: "🤖 Chat with Bot ", type: ActionTypes.ImBack, value: "Book some flight tickets"),
                            new CardAction(title: "☎️ Call Us 24/7", type: ActionTypes.Call, value: "+18888898005"),
                            new CardAction(title: "✈️ Search Results ", type: ActionTypes.OpenUrl, value: CarouselResult.Data[1].ApiUrl.ToString())
                        })
                    .ToAttachment(),

                    new HeroCard(
                        title: "We are a travel agency trusted over 30 years, with 95 % positive customer reviews and A+ rating from BBB",
                        images: new CardImage[] { new CardImage(url: CarouselResult.Data[2].ImageUrl.ToString()) },
                        buttons: new CardAction[]
                        {
                           new CardAction(title: "🤖 Chat with Bot ", type: ActionTypes.ImBack, value: "Book some flight tickets"),
                            new CardAction(title: "☎️ Call Us 24/7", type: ActionTypes.Call, value: "+18888898005"),
                            new CardAction(title: "✈️ Search Results ", type: ActionTypes.OpenUrl, value: CarouselResult.Data[2].ApiUrl.ToString())
                        })
                    .ToAttachment()


                    });
                        /////Printing carousels for One way trips here.
                        
                        await stepContext.Context.SendActivityAsync(activity1, cancellationToken: cancellationToken);

                    }

                    /////  Flushing the states here for one way trip
                    //////////////////////////////////////////
                    state.Origin = null;
                    state.Destination = null;
                    state.FlyDate = null;
                    return await stepContext.EndDialogAsync();

                }

              

                return await stepContext.NextAsync();

            });

            AddStep(async (stepContext, cancellationToken) =>
            {
                var state = await (stepContext.Context.TurnState["TraveloAccessors"] as TraveloAccessors).TraveloStateStateAccessor.GetAsync(stepContext.Context, new System.Func<TraveloState>(() => new TraveloState()));

                if (state.ReturnDate == null && stepContext.Result.ToString() != "No")
                {
                    List<DateTimeResolution> re = (List<DateTimeResolution>)stepContext.Result;
                    state.ReturnDate = re.FirstOrDefault().Value;
                    await stepContext.Context.SendActivityAsync($"Let us show you the best deals we have got for your round trip from {state.Origin} to {state.Destination} for the travel dates {state.FlyDate} - {state.ReturnDate}, per traveler in Economy class");
                }
                else
                {
                    await stepContext.Context.SendActivityAsync($"Let us show you the best deals we have got for {state.Origin} to {state.Destination} for the travel dates {state.FlyDate} - {state.ReturnDate} per person in Economy class");
                }

                return await stepContext.NextAsync();

            });

            AddStep(async (stepContext, cancellationToken) =>
            {
                ///making an api call to retrieve carousels for Round trip.
                ///
                var state = await (stepContext.Context.TurnState["TraveloAccessors"] as TraveloAccessors).TraveloStateStateAccessor.GetAsync(stepContext.Context, new System.Func<TraveloState>(() => new TraveloState()));
                var client = new RestClient("https://stagingapi.travelomind.com/");
                var request = new RestRequest($"api/Flights/carousel/summary?to={state.Destination}&from={state.Origin}&departDate={state.FlyDate}&returnDate={state.ReturnDate}&adult=1&child=0&infant=0&queryType=1&roundTrip=TRUE&classOfTravel=Y&userName=GOD", Method.GET);
                var queryResult = client.Execute(request).Content;
                CarouselMap CarouselResult = JsonConvert.DeserializeObject<CarouselMap>(queryResult);

           if (CarouselResult.Status)
           {
                    // Create an attachment. Add the carousels to it.
                    

                    var activity1 = MessageFactory.Carousel(new Attachment[]
                {
                    new HeroCard(
                        title: "We are a travel agency trusted over 30 years, with 95 % positive customer reviews and A+ rating from BBB",
                        images: new CardImage[] { new CardImage(url: CarouselResult.Data[0].ImageUrl.ToString())},
                        buttons: new CardAction[]
                        {
                            new CardAction(title: "🤖 Chat with Bot ", type: ActionTypes.ImBack, value: "Book some flight tickets"),
                            new CardAction(title: "☎️ Call Us 24/7", type: ActionTypes.Call, value: "+18888898005"),
                            new CardAction(title: "✈️ Search Results ", type: ActionTypes.OpenUrl, value: CarouselResult.Data[0].ApiUrl.ToString())

                        })
                    .ToAttachment(),
                    new HeroCard(
                        title: "We are a travel agency trusted over 30 years, with 95 % positive customer reviews and A+ rating from BBB",
                        images: new CardImage[] { new CardImage(url: CarouselResult.Data[1].ImageUrl.ToString()) },
                        buttons: new CardAction[]
                        {
                           new CardAction(title: "🤖 Chat with Bot ", type: ActionTypes.ImBack, value: "Book some flight tickets"),
                            new CardAction(title: "☎️ Call Us 24/7", type: ActionTypes.Call, value: "+18888898005"),
                            new CardAction(title: "✈️ Search Results ", type: ActionTypes.OpenUrl, value: CarouselResult.Data[1].ApiUrl.ToString())
                        })
                    .ToAttachment(),

                    new HeroCard(
                        title: "We are a travel agency trusted over 30 years, with 95 % positive customer reviews and A+ rating from BBB",
                        images: new CardImage[] { new CardImage(url: CarouselResult.Data[2].ImageUrl.ToString()) },
                        buttons: new CardAction[]
                        {
                           new CardAction(title: "🤖 Chat with Bot ", type: ActionTypes.ImBack, value: "Book some flight tickets"),
                            new CardAction(title: "☎️ Call Us 24/7", type: ActionTypes.Call, value: "+18888898005"),
                            new CardAction(title: "✈️ Search Results ", type: ActionTypes.OpenUrl, value: CarouselResult.Data[2].ApiUrl.ToString())
                        })
                    .ToAttachment()


                });
                    // Printing carousels for Return trips here
                    await stepContext.Context.SendActivityAsync(activity1, cancellationToken: cancellationToken);

                }
                
                return await stepContext.NextAsync();
            });


            AddStep(async (stepContext, cancellationToken) =>
            {
                var state = await (stepContext.Context.TurnState["TraveloAccessors"] as TraveloAccessors).TraveloStateStateAccessor.GetAsync(stepContext.Context, new System.Func<TraveloState>(() => new TraveloState()));
                state = new TraveloState();

                //state.Destination = null;
                //state.FlyDate = null;
                //state.ReturnDate = null;

                return await stepContext.EndDialogAsync();

            });

                AddStep(async (stepContext, cancellationToken) => { return await stepContext.ReplaceDialogAsync(Id); });
           
        }

       
        public static string Id => "mainDialog";

        //public static MainDialog Instance { get; } = new MainDialog(Id,entityMap);

       
    }
}