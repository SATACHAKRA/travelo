﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;


namespace Travelo
{   

        public partial class EntityMap
        {
            [JsonProperty("query")]
            public string Query { get; set; }

            [JsonProperty("topScoringIntent")]
            public TopScoringIntent TopScoringIntent { get; set; }

            [JsonProperty("entities")]
            public Entity[] Entities { get; set; }
            //public List<Entity> Entities { get; set; }
        
            [JsonProperty("intents")]
            public object Intents { get; set; }

         }

        public partial class Entity
        {
            [JsonProperty("entity")]
            public string EntityEntity { get; set; }

            [JsonProperty("type")]
            public string Type { get; set; }

            [JsonProperty("startIndex")]
            public long StartIndex { get; set; }

            [JsonProperty("endIndex")]
            public long EndIndex { get; set; }

            [JsonProperty("score")]
            public double Score { get; set; }

            [JsonProperty("resolution")]
            public Resolution Resolution { get; set; }
        }

        public partial class Resolution
        {
            [JsonProperty("values")]
            public object Values { get; set; }

            [JsonProperty("unit")]
            public object Unit { get; set; }

            [JsonProperty("value")]
            public string Value { get; set; }
        }

        public partial class TopScoringIntent
        {
            [JsonProperty("intent")]
            public string Intent { get; set; }

            [JsonProperty("score")]
            public long Score { get; set; }
        }



}

////////////////////     Various Json Entity Maps    //////////////////////
/*
 *  ///////////////////// For LUIS Entity Handling ///////////////////////////

       public partial class EntityMap
       {
           [JsonProperty("$instance")]
           public Instance Instance { get; set; }

           [JsonProperty("datetime")]
           public EntityMapDatetime[] Datetime { get; set; }

           [JsonProperty("destination_city")]
           public string[] DestinationCity { get; set; }

           [JsonProperty("source_city")]
           public string[] SourceCity { get; set; }

       public Dictionary<string, string> getMapping
       {           
           get
           {
               Dictionary<string, string> EntDic = new Dictionary<string, string>();
               EntDic.Add("source_city", SourceCity[0]);
               EntDic.Add("destination_city", DestinationCity[0]);
               for(int i=0;i<Datetime.Length;i++)
               {

                       EntDic.Add("Datetime" + (i + 1).ToString(), Datetime[i].Timex.GetValue(0).ToString());

               }
               return EntDic;


           }

       }
   }

       public partial class EntityMapDatetime
       {
           [JsonProperty("type")]
           public string Type { get; set; }

           [JsonProperty("timex")]
           public string[] Timex { get; set; }

           [JsonProperty("value")]
           public string Value { get; set; }
   }

       public partial class Instance
       {
           [JsonProperty("datetime")]
           public DestinationCityElement[] Datetime { get; set; }

           [JsonProperty("destination_city")]
           public DestinationCityElement[] DestinationCity { get; set; }

           [JsonProperty("source_city")]
           public DestinationCityElement[] SourceCity { get; set; }
       }

       public partial class DestinationCityElement
       {
           [JsonProperty("startIndex")]
           public long StartIndex { get; set; }

           [JsonProperty("endIndex")]
           public long EndIndex { get; set; }

           [JsonProperty("text")]
           public string Text { get; set; }

           [JsonProperty("type")]
           public string Type { get; set; }

           [JsonProperty("score", NullValueHandling = NullValueHandling.Ignore)]
           public double? Score { get; set; }
       }
 
 */


//////////////////// Travomind Entity Map //////////////////////////

/*
public class EntityMap
{

    public string query { get; set; }
    public Intent topScoringIntent { get; set; }
    public List<Entity> entities { get; set; }
    public List<Intent> intents { get; set; }

   public Dictionary<string, string> getMapping
       {
           get
           {
               return new Dictionary<string, string>()
               {
                   {"source_city",source_city },
                   {"destination_city",destination_city}
               };
           }
       
}

public class Entity
{
    public string entity { get; set; }
    public string type { get; set; }
    public int startIndex { get; set; }
    public int endIndex { get; set; }
    public double score { get; set; }
    public Resolution resolution { get; set; }
}

public class Resolution
{
    public List<ResolvedValue> values { get; set; }
    public string unit { get; set; }
    public string value { get; set; }
}

public class ResolvedValue
{
    public string timex { get; set; }
    public string type { get; set; }
    public string Mod { get; set; }
    public string start { get; set; }
    public string end { get; set; }
    public string value { get; set; }

}

public class Intent
{
    public string intent { get; set; }
    public double score { get; set; }
}

*/







