﻿using System;
using System.Collections.Generic;

using Microsoft.Bot.Builder.Dialogs;

namespace Travelo
{
    public class EndDialog:WaterfallDialog
    {
        public EndDialog(string dialogId, IEnumerable<WaterfallStep> steps = null) : base(dialogId, steps)
        {
            AddStep(async (stepContext, cancellationToken) =>
            {

                await stepContext.Context.SendActivityAsync("You chose to cancel this operation. What can I do for you? ");
                return await stepContext.EndDialogAsync();


            });
        }

        public static string Id => "EndDialog";

        public static EndDialog Instance { get; } = new EndDialog(Id);
    }
}
