﻿using Microsoft.Bot.Builder.Dialogs;
using System.Collections.Generic;



namespace Travelo
{
    public class greetingsDialog:WaterfallDialog
    {
        public greetingsDialog(string dialogId, IEnumerable<WaterfallStep> steps = null) : base(dialogId, steps)
        {
            AddStep(async (stepContext, cancellationToken) =>
            {

                await stepContext.Context.SendActivityAsync("Hello there.!!! I am a Travel-🤖. Type your travel queries below. ");
               return await stepContext.EndDialogAsync();


            });
        }

        public static string Id => "greetingsDialog";

        public static greetingsDialog Instance { get; } = new greetingsDialog(Id);
    }
}
